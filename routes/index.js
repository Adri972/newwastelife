var express = require('express');
var moment = require('moment');
var router = express.Router();
var jwtDecode = require('jwt-decode');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const mongoose = require('mongoose');
const Annonce = mongoose.model('Annonces');
const User = mongoose.model('User');
const nodemailer = require("nodemailer");
var _ = require('lodash');
const {
    google
} = require("googleapis");
const hbs = require('nodemailer-express-handlebars');
const blacklist = require('../config/blacklist');

// Configuration du client mail
var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    auth: {
        user: "newwastelife@gmail.com",
        pass: "newwastelife6150"
    },
});

var handlebarOptions = {
    viewEngine: {
        extName: '.hbs',
        layoutsDir: 'views/email/',
        partialsDir: 'views/partials/',
        defaultLayout: 'template'
    },
    viewPath: './views/email',
    extName: '.handlebars',
};

transporter.use('compile', hbs(handlebarOptions));

// Vérifie si l'utilisateur est connecté
var isAuthenticated = function (req, res, next) {
    const username = getUsernameFromCookie(req.cookies.jwt);
    User.findOne({
        username: username
    }).then(user => {
        if (user) {
            req.user = user;
            next();
        } else {
            if (req.url === '/favorite') {
                return res.send({
                    error: '/login'
                })
            } else {
                return res.redirect("/login");
            }
        }
    }).catch(err => {
        console.log(err);
        return res.status(500).send('Une erreur est survenue...');
    });
}

// Vérifie si l'utilisateur n'est pas connecté
var notAuthenticated = function (req, res, next) {
    const username = getUsernameFromCookie(req.cookies.jwt);

    User.findOne({
        username: username
    }).then(user => {
        if (user) {
            req.user = user;
            res.redirect("/profile");
        } else {
            next();
        }
    }).catch(err => {
        console.log(err);
        return res.status(500).send('Une erreur est survenue...');
    });;
}

// Vérifie l'utilisateur actuel
var isCurrentUser = function (req, res, next) {
    const username = getUsernameFromCookie(req.cookies.jwt);

    if (username && username === req.params.username) {
        return res.redirect("/profile");
    } else {
        User.findOne({
            username: username
        }).then(user => {
            req.curruser = user;
            next();
        }).catch(err => {
            console.log(err);
            return res.status(500).send('Une erreur est survenue...');
        });
    }
}

// Decode le cookie pour récupérer le username
function getUsernameFromCookie(token) {
    var username = "";
    if (token) {
        username = jwtDecode(token).id
    }

    return username;
}

// Créer un token pour le cookie
function createTokenForCookie(username) {
    const payload = {
        id: username
    };
    const token = jwt.sign(payload, keys.secretOrKey, {
        expiresIn: 3600
    });

    return token;
}

// Vérifie si la description contient des mots blacklisted
function descriptionIsValid(description) {
    let isValid = true;
    for (let i = 0; i < blacklist.length; i++) {
        if (description.includes(blacklist[i])) {
            isValid = false;
            break;
        }
    }
    return isValid;
}

// Entreposage des photos d'annonce localement, le temps de trouver une solution en ligne
const multer = require('multer');
const storage = multer.diskStorage({
    destination: './uploads/',
    filename: function (req, file, cb) {
        let filename = (Math.random().toString(10).substring(5)) + Date.now();
        let fExtension = (file.originalname).substring(((file.originalname).lastIndexOf('.')));
        cb(null, filename + fExtension);
    }
});
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 25
    }
});

moment.locale('fr');
// Route qui affiche la page d'accueil
router.get("/", function (req, res) {
    const username = getUsernameFromCookie(req.cookies.jwt);

    User.findOne({
            username: username
        })
        .populate('annonces')
        .exec((err, user) => {
            if (err) {
                Annonce.find({}, (err, annonces) => {
                    if (err) {
                        console.log(err)
                    } else {
                        res.render("home", {
                            annonces: annonces,
                            user: ""
                        })
                    }
                })
            } else {
                Annonce.find({}, (err, annonces) => {
                    if (err) {
                        console.log(err)
                    } else {
                        res.render("home", {
                            annonces: annonces,
                            user: user
                        })
                    }
                })
            }
        })
});

// Route AJAX qui permet de filtrer les annonces par prix et par type
router.post('/sortPrice/:info', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;
    var url = req.params.info.split("_");
    var type = url[0];
    var min = url[1];
    var max = url[2];

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    Annonce.find({
        type: type
    }).sort('price').exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            var annonces2 = [];
            if (max != "") {
                for (var i = 0; i < annonces.length; i++) {
                    if (parseInt(annonces[i].price) >= parseInt(min)) {
                        if (parseInt(annonces[i].price) <= parseInt(max)) {
                            annonces2.push(annonces[i]);
                        }
                    }
                }
            } else {
                annonces2 = annonces;
            }

            res.render("annonces_ajax", {
                results: annonces2,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    });
});

// Route AJAX qui permet de filtrer les annonces par prix de tout type
router.post('/sortPriceAll/:info', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;
    var url = req.params.info.split("_");
    var min = url[0];
    var max = url[1];

    if (token) {
        username = jwtDecode(token).id
    }

    const user = await
    User.findOne({
        username: username
    });

    var annonces = JSON.parse(JSON.stringify(req.body));
    var list_id = [];

    for (var i = 0; i < annonces.length; i++) {
        for (var j = 0; j < annonces[i].length; j++) {
            list_id.push(annonces[i][j]._id);
        }
    }


    Annonce.find({
        '_id': {
            $in: list_id
        }
    }).sort('price').exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            var annonces2 = [];

            for (var i = 0; i < annonces.length; i++) {
                if (parseInt(annonces[i].price) >= parseInt(min)) {
                    if (parseInt(annonces[i].price) <= parseInt(max)) {
                        annonces2.push(annonces[i]);
                    }
                }
            }

            res.render("annonces_ajax", {
                results: annonces2,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de filtrer les annonces par dates ascendantes et par type
router.post('/sortDateAsc/:type', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }

    const user = await
    User.findOne({
        username: username
    });

    Annonce.find({
        type: req.params.type
    }).sort('-date').exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    });
})

// Route AJAX qui permet de filtrer les annonces par dates ascendantes de tout type
router.post('/sortDateAscAll', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }

    const user = await
    User.findOne({
        username: username
    });

    var annonces = JSON.parse(JSON.stringify(req.body));
    var list_id = [];

    for (var i = 0; i < annonces.length; i++) {
        for (var j = 0; j < annonces[i].length; j++) {
            list_id.push(annonces[i][j]._id);
        }
    }


    Annonce.find({
        '_id': {
            $in: list_id
        }
    }).sort('-date').exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    });
})

// Route AJAX qui permet de filtrer les annonces par dates descendantes et par type
router.post('/sortDateDesc/:type', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    Annonce.find({
        type: req.params.type
    }).sort('date').exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de filtrer les annonces par dates descendantes de tout type
router.post('/sortDateDescAll', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }

    const user = await
    User.findOne({
        username: username
    });

    var annonces = JSON.parse(JSON.stringify(req.body));
    var list_id = [];

    for (var i = 0; i < annonces.length; i++) {
        for (var j = 0; j < annonces[i].length; j++) {
            list_id.push(annonces[i][j]._id);
        }
    }

    Annonce.find({
        '_id': {
            $in: list_id
        }
    }).sort('date').exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    });
})

// Route AJAX qui permet de ré-initialiser le filtre par date
router.post('/sortDateAll', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    var annonces = JSON.parse(JSON.stringify(req.body));
    var list_id = [];

    for (var i = 0; i < annonces.length; i++) {
        for (var j = 0; j < annonces[i].length; j++) {
            list_id.push(annonces[i][j]._id);
        }
    }


    Annonce.find({
        '_id': {
            $in: list_id
        },
    }).exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de filtrer les annonces ayant des photos et par type
router.post('/sortPhotos/:type', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    Annonce.find({
        type: req.params.type,
        photos: {
            $exists: true,
            $ne: []
        }
    }).exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de filtrer les annonces ayant des photos de tout type
router.post('/sortPhotos', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    var annonces = JSON.parse(JSON.stringify(req.body));
    var list_id = [];


    for (var i = 0; i < annonces.length; i++) {
        for (var j = 0; j < annonces[i].length; j++) {
            list_id.push(annonces[i][j]._id);
        }
    }


    console.log(list_id);

    Annonce.find({
        '_id': {
            $in: list_id
        },
        'photos': {
            $exists: true,
            $ne: []
        }
    }).exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de filtrer les annonces n'ayant de photos et par type
router.post('/sortNoPhotos/:type', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    Annonce.find({
        type: req.params.type,
        photos: {
            $exists: true,
            $eq: []
        }
    }).exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de filtrer les annonces n'ayant pas de photos et de tout type
router.post('/sortNoPhotos', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    var annonces = JSON.parse(JSON.stringify(req.body));
    var list_id = [];


    for (var i = 0; i < annonces.length; i++) {
        for (var j = 0; j < annonces[i].length; j++) {
            list_id.push(annonces[i][j]._id);
        }
    }

    Annonce.find({
        '_id': {
            $in: list_id
        },
        'photos': {
            $exists: true,
            $eq: []
        }
    }).exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de lister toute les annonces d'un type
router.post('/allAds/:type', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    Annonce.find({
        type: req.params.type
    }).exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de lister tout les annonces
router.post('/allAds', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    Annonce.find().exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de filtrer les annonces par type
router.post('/sortCateg/:type', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    var annonces = JSON.parse(JSON.stringify(req.body));
    var list_id = [];


    for (var i = 0; i < annonces.length; i++) {
        for (var j = 0; j < annonces[i].length; j++) {
            list_id.push(annonces[i][j]._id);
        }
    }


    console.log(list_id);

    Annonce.find({
        '_id': {
            $in: list_id
        },
        'type': req.params.type
    }).exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Route AJAX qui permet de ré-initialiser le filtre par type
router.post('/sortCategAll', async function (req, res) {
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await
    User.findOne({
        username: username
    });

    var annonces = JSON.parse(JSON.stringify(req.body));
    var list_id = [];


    for (var i = 0; i < annonces.length; i++) {
        for (var j = 0; j < annonces[i].length; j++) {
            list_id.push(annonces[i][j]._id);
        }
    }


    console.log(list_id);

    Annonce.find({
        '_id': {
            $in: list_id
        },
    }).exec(function (err, annonces) {
        if (err) {
            console.log(err)
        } else {
            res.render("annonces_ajax", {
                layout: false,
                results: annonces,
                user: user
            }, function (err, html) {
                var response = {
                    my_html: html
                };
                res.send(response);
            });
        }
    })
})

// Page affichant tout les annonces des biens
router.get("/biens", function (req, res) {
    const username = getUsernameFromCookie(req.cookies.jwt);

    User.findOne({
            username: username
        })
        .populate('annonces')
        .exec((err, user) => {
            if (err) {
                11
            } else {
                Annonce.find({
                    type: 'Bien'
                }, (err, annoncesBiens) => {
                    if (err) {
                        console.log(err)
                    } else {
                        res.render("biens", {
                            annoncesBiens: annoncesBiens,
                            user: user
                        })
                    }
                })
            }
        })
})

// Page affichant tout les annonces des services
router.get("/services", function (req, res) {
    const username = getUsernameFromCookie(req.cookies.jwt);

    User.findOne({
            username: username
        })
        .populate('annonces')
        .exec((err, user) => {
            if (err) {
                Annonce.find({
                    type: 'Service'
                }, (err, annoncesServices) => {
                    if (err) {
                        console.log(err)
                    } else {
                        res.render("services", {
                            annoncesServices: annoncesServices,
                            user: ""
                        })
                    }
                })
            } else {
                Annonce.find({
                    type: 'Service'
                }, (err, annoncesServices) => {
                    if (err) {
                        console.log(err)
                    } else {
                        res.render("services", {
                            annoncesServices: annoncesServices,
                            user: user
                        })
                    }
                })
            }
        })
})

// Route qui permet d'être redirigé vers la page d'authentification
router.get("/login", notAuthenticated, function (req, res) {
    res.render("login", {
        errorMsg: "",
        error: ""
    });
});

// Route qui permet de vérifier si l'utilisateur existe dans la base de donnée
router.post("/login", function (req, res) {
    const email = req.body.inputEmail;
    const password = req.body.inputPassword;

    User.findOne({
        courriel: email
    }, function (err, user) {
        if (err) {
            console.log(err);
            return res.status(500).send('Une erreur est survenue...');
        }
        if (user) {
            bcrypt.compare(password, user.password, function (err, compareResult) {
                if (err) {
                    console.log(err);
                    return res.status(500).send('Une erreur est survenue...');
                }
                if (compareResult) {
                    res.cookie('jwt', createTokenForCookie(user.username), {
                        encode: String
                    });
                    res.redirect('/');
                } else
                    res.render("login", {
                        errorMsg: "Mot de passe incorrect",
                        error: "password",
                        email: email
                    });
            });
        } else
            res.render("login", {
                errorMsg: "Courriel inexistant",
                error: "email",
                email: email
            });
    });
});

// Route qui permet la déconnexion du compte authentifié
router.get("/logout", isAuthenticated, function (req, res, next) {
    res.clearCookie("jwt");
    console.log("Cookie supprimé");
    res.redirect('/');
});

// Route qui affiche le profil des autres utilisateurs du site web
router.get("/user/:username", isCurrentUser, function (req, res) {
    const username = req.params.username;

    User.findOne({
            username: username
        })
        .populate('annonces')
        .exec((err, userpage) => {
            if (err) {
                console.log(err);
                return res.status(500).send('Une erreur est survenue...');
            } else {
                if (userpage) {
                    return res.render("user", {
                        user: req.curruser,
                        userpage: userpage
                    });
                } else {
                    res.status(404).send("L'utilisateur n'existe pas!");
                }
            }
        });
});

// Route qui affiche le profil de l'utilisateur connecté
router.get("/profile", isAuthenticated, function (req, res) {
    User.findOne({
            username: req.user.username
        })
        .populate('annonces')
        .populate('favorites')
        .exec((err, user) => {
            if (err) {
                console.log(err);
                return res.status(500).send('Une erreur est survenue...');
            } else {
                res.render("profile", {
                    user: user
                });
            }
        });
});

// Route qui vérifie et applique les modifications au profil
router.post("/profile", function (req, res) {
    var newUsername = req.body.username;
    var oldpassword = req.body.oldpassword;
    var newpassword = req.body.newpassword;
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;
    var address = req.body.address;
    var city = req.body.city;
    var province = req.body.province;
    var postalcode = req.body.postalcode;
    var phone = req.body.phonenumber;

    const username = getUsernameFromCookie(req.cookies.jwt);

    User.findOne({
        username: username
    }, async (err, user) => {
        if (err) {
            console.log(err);
            return res.sendStatus(500);
        } else {
            if (user.firstname !== firstname && firstname) {
                user.firstname = firstname;
            }
            if (user.lastname !== lastname && lastname) {
                user.lastname = lastname;
            }
            if (user.username !== newUsername && newUsername) {
                const userExist = await User.findOne({
                    username: newUsername
                }).catch(err => {
                    console.log(err);
                    return res.sendStatus(500);
                });
                if (userExist) {
                    res.send({
                        error: "username"
                    })
                } else {
                    user.username = newUsername;
                    res.clearCookie("jwt");

                    res.cookie('jwt', createTokenForCookie(newUsername), {
                        encode: String
                    });
                }
            }
            if (user.address !== address && address) {
                user.address = address;
            }
            if (user.city !== city && city) {
                user.city = city;
            }
            if (user.province !== province && province) {
                user.province = province;
            }
            if (user.postalcode !== postalcode && postalcode) {
                user.postalcode = postalcode;
            }
            if (user.phone !== phone && phone) {
                user.phone = phone;
            }
            if (newpassword) {
                bcrypt.compare(oldpassword, user.password, function (err, compareResult) {
                    if (err) {
                        console.log(err);
                        return res.sendStatus(500);
                    }
                    if (compareResult) {
                        bcrypt.genSalt(10, (err, salt) => {
                            if (err) {
                                console.log(err);
                                return res.sendStatus(500);
                            }
                            bcrypt.hash(newpassword, salt, (err, hash) => {
                                if (err) {
                                    console.log(err);
                                    return res.sendStatus(500);
                                }
                                user.password = hash;
                                user.save();
                                res.send();
                            });
                        });
                    } else {
                        res.send({
                            error: "password"
                        });
                    }
                });
            } else {
                user.save();
                res.send();
            }
        }
    });
});

// Page affichant un formulaire de création de compte utilisateur
router.get("/register", notAuthenticated, function (req, res) {
    const username = getUsernameFromCookie(req.cookies.jwt);

    User.findOne({
        username: username
    }).then(users => {
        var user = "";
        if (users) {
            user = users.username;
        }
        res.render("register", {
            username: user
        })
    })
});

// Route qui permet de traiter les données envoyées à partir du formulaire d'inscription
router.post("/register", async function (req, res) {
    var username = req.body.username;
    var courriel = req.body.email;
    var password = req.body.password;
    var password2 = req.body.password2;
    var prenom = req.body.firstname;
    var nom = req.body.lastname;
    var address = req.body.address;
    var city = req.body.city;
    var province = req.body.province;
    var postalcode = req.body.postalcode;
    var phone = req.body.phonenumber;


    var error = "";
    var errorType = "";

    const user = await User.findOne({
        courriel: courriel
    })

    if (user) {
        error += "Courriel indisponible";
        errorType += "email";
    }

    if (username == "" || prenom == "" || nom == "" || address == "" || city == "" ||
        province == "" || postalcode == "") {
        error += 'Veuillez remplir tout les champs obligatoire !';
        errorType += "inputs";
    }

    if (password != password2) {
        error += "Mots de passes différents";
        errorType += "passwords";
    }

    // Si une erreur est détecté, l'internaute est rediriger vers la route register
    if (error != "") {
        var infos = {
            nom: nom,
            prenom: prenom,
            username: username,
            address: address,
            city: city,
            province: province,
            postalcode: postalcode,
            courriel: courriel,
            phone: phone
        };

        console.log(errorType);
        console.log(error);

        return res.render("register", {
            error: error,
            errorType: errorType,
            infos: infos
        });
    }

    // Objet contenant les informations nécessaires à la création d'un utilisateur
    const newUser = new User({
        username: username,
        firstname: prenom,
        lastname: nom,
        courriel: courriel,
        address: address,
        city: city,
        province: province,
        postalcode: postalcode,
        phone: phone,
        password: password
    });

    // Génère une chaîne aléatoire et créer un hash à partir de la chaîne et du mot de passe
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            // Création de l'utilisateur dans la Base de données
            newUser.save();
        });
    });

    const payload = {
        id: username,
    }; // Create JWT Payload

    // Sign Token
    var token = jwt.sign(
        payload,
        keys.secretOrKey, {
            expiresIn: 3600
        });
    res.cookie('jwt', token, {
        encode: String
    });

    // Envoi du courriel de confirmation
    var options = {
        from: "New Waste Life <newwastelife@gmail.com>",
        to: newUser.courriel,
        subject: 'Confirmation de votre inscription',
        template: 'contact-register',
        context: {
            user: newUser
        }
    };

    transporter.sendMail(options, function (err, info) {
        if (err) {
            console.log(err);
            res.redirect('/confirmation/register/failed');
        } else {
            console.log(info);
            res.redirect('/confirmation/register/success');
        }
    });
});

// Route qui permet la création d'une annonce
router.get('/create-ad', isAuthenticated, function (req, res) {
    res.render("create-ad", {
        user: req.user
    });
});

// Route qui traite les données pour la création d'une annonce
router.post('/create-ad', isAuthenticated, upload.array('adphoto', 3), function (req, res) {
    const title = req.body.title;
    const price = req.body.price;
    const description = req.body.description;
    const type = req.body.type;
    const photos = (req.files).map(file => file.filename);

    const user = req.user;

    var verif = !descriptionIsValid(description);

    if (user) {
        const newAnnonce = new Annonce({
            author: user._id,
            title: title[0],
            price: price[0],
            description: description,
            type: type[0],
            photos: photos,
            acheter: "",
            verif: verif
        });

        newAnnonce.save().then(annonce => {
            user.annonces.push(annonce._id);
            user.save().catch(err => {
                console.log(err)
            });
        });

        if (descriptionIsValid(description)) {
            console.log(descriptionIsValid(description));
            // Envoi d'un courriel quand l'annonce est valide
            var options = {
                from: "New Waste Life <newwastelife@gmail.com>",
                to: user.courriel,
                subject: 'Votre annonce ' + newAnnonce.title + ' est en ligne !',
                template: 'contact-new-ad',
                context: {
                    user: {
                        lastname: user.lastname,
                        firstname: user.firstname
                    },
                    annonceTitle: newAnnonce.title
                }
            };

            transporter.sendMail(options, function (err, info) {
                if (err) {
                    console.log(err);
                    res.redirect('/confirmation/newAd/failed')
                } else {
                    console.log(info);
                    res.redirect('/confirmation/newAd/success')
                }
            });
            // FIN 
        } else {
            // Envoi d'un courriel quand l'annonce n'est pas valide
            var options = {
                from: "New Waste Life <newwastelife@gmail.com>",
                to: user.courriel,
                subject: 'Votre annonce est en revue pour vérifier son contenu !',
                template: 'contact-review-ad',
                context: {
                    user: user
                }
            };

            transporter.sendMail(options, function (err, info) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(info);
                }
            });

            var options2 = {
                from: "New Waste Life <newwastelife@gmail.com>",
                to: "New Waste Life <newwastelife@gmail.com>",
                subject: "Vérification d'une annonce",
                template: 'contact-review-ad-moderate',
                context: {
                    user: user,
                    annonce: newAnnonce
                }
            };
    
            transporter.sendMail(options2, function (err, info) {
                if (err) {
                    console.log(err);
                    res.redirect('/confirmation/newAdReview/failed');
                } else {
                    console.log(info);
                    res.redirect('/confirmation/newAdReview/success');
                }
            });
        }
    } else {
        res.redirect("/confirmation/newAd/failed");
    }
});

// Route qui affiche l'annonce selon son id
router.get('/ad/:id', async function (req, res) {
    var id = req.params.id;

    const user = await User.findOne({
        username: getUsernameFromCookie(req.cookies.jwt)
    }).catch(err => {
        console.log(err);
        return res.status(500).send('Une erreur est survenue...');
    });

    Annonce.findById(id).populate('author').exec((err, annonce) => {
        if (err) {
            res.status(404).send("404: Annonce non existante");
        } else {
            let favorite = user ? user.favorites.includes(annonce._id) : false;
            res.render("ad", {
                adId: annonce.id,
                adTitle: annonce.title,
                adPrice: annonce.price,
                adDescription: annonce.description,
                adAuthorId: annonce.author._id,
                adAuthorName: annonce.author.username,
                adAuthorPostalCode: annonce.author.postalcode,
                adPhotos: annonce.photos,
                adDate: moment(annonce.date).format('DD MMMM YYYY'),
                user: user,
                message: "",
                messageType: "",
                favorite: favorite,
                acheter: annonce.acheter,
                address: annonce.author.address,
                verif: annonce.verif
            });
        }
    });
});

// Route qui affiche un message de confirmation pour le succès ou l'échec d'une inscription, 
// de la création d'une annonce et la modification d'une annonce
router.get("/confirmation/:type/:status", function (req, res) {
    const type = req.params.type;
    const status = req.params.status;

    User.findOne({
        username: getUsernameFromCookie(req.cookies.jwt)
    }).then(user => {
        res.render("confirmation", {
            user: user,
            confirmation: type,
            status: status
        });
    }).catch(err => {
        console.log(err);
        return res.status(500).send('Une erreur est survenue...');
    });

});

// Route qui traite les informations pour envoyer un message au propriétaire d'une annonce depuis une annonce
router.post("/contact/ad/:ad_id", function (req, res) {
    const ad_id = req.params.ad_id;
    const sender_name = req.body.name;
    const sender_courriel = req.body.courriel;
    const sender_message = req.body.message;

    var message = "";
    var messageType = "";

    if (sender_name == "" || sender_courriel == "" || sender_message == "") {
        message += 'Veuillez remplir tout les champs obligatoire !';
        messageType += "inputs";
    }

    if (message != "") {
        res.send({
            message: message,
            messageType: messageType
        });
    }

    Annonce.findById(ad_id).populate('author').exec((err, annonce) => {
        if (err) {
            message += "Une erreur est survenue ! Veuillez réessayer. <br>Si le problème persiste, veuillez nous contacter !";
            messageType = "error";
            res.status(500).send({
                message: message,
                messageType: messageType
            });
        } else {
            const options = {
                from: "New Waste Life <newwastelife@gmail.com>",
                to: annonce.author.courriel,
                subject: 'Nouveau message de ' + sender_name,
                template: 'contact-ad',
                context: {
                    user: {
                        lastname: annonce.author.lastname,
                        firstname: annonce.author.firstname
                    },
                    annonce: annonce,
                    sender: {
                        name: sender_name,
                        courriel: sender_courriel,
                        message: sender_message,
                    }
                }
            };

            transporter.sendMail(options, function (err, info) {
                if (err) {
                    console.log(err);
                    message += "Une erreur est survenue ! Veuillez réessayer. <br>Si le problème persiste, veuillez nous contacter !";
                    messageType = "error";
                } else {
                    console.log(info);
                    message += "Message envoyé avec succés !";
                    messageType = "success";
                }
                res.send({
                    message: message,
                    messageType: messageType
                });
            });
        }
    });
});

router.post("/contact/user/:username", async function (req, res) {
    var seller_username = req.params.username;
    var sender_name = req.body.name;
    var sender_courriel = req.body.courriel;
    var sender_message = req.body.message;

    var message = "";
    var messageType = "";

    if (sender_name == "" || sender_courriel == "" || sender_message == "") {
        message += 'Veuillez remplir tout les champs obligatoire !';
        messageType += "inputs";
    }

    if (message != "") {
        res.send({
            message: message,
            messageType: messageType
        });
    }

    User.findOne({
            username: seller_username
        })
        .populate('annonces')
        .exec((err, user) => {
            if (err) {
                message += "Une erreur est survenue ! Veuillez réessayer. <br>Si le problème persiste, veuillez nous contacter !";
                messageType = "error";
                res.send({
                    message: message,
                    messageType: messageType
                });
            } else {
                var options = {
                    from: "New Waste Life <newwastelife@gmail.com>",
                    to: user.courriel,
                    subject: 'Nouveau message de ' + sender_name,
                    template: 'contact-user',
                    context: {
                        user: {
                            lastname: user.lastname,
                            firstname: user.firstname
                        },
                        sender: {
                            name: sender_name,
                            courriel: sender_courriel,
                            message: sender_message
                        }
                    }
                };

                transporter.sendMail(options, function (err, info) {
                    if (err) {
                        console.log(err);
                        message += "Une erreur est survenue ! Veuillez réessayer. <br>Si le problème persiste, veuillez nous contacter !";
                        messageType = "error";
                    } else {
                        console.log(info);
                        message += "Message envoyé avec succés !";
                        messageType = "success";
                    }
                    res.send({
                        message: message,
                        messageType: messageType
                    });
                });
            }
        });
});

router.post("/delete/ad/:id", async function (req, res) {
    var ad_id = req.params.id;
    var username = "";
    var token = req.cookies.jwt;

    if (token) {
        username = jwtDecode(token).id
    }
    const user = await User.findOne({
        username: username
    });

    Annonce.findById(ad_id).populate('author').exec((err, annonce) => {
        if (err) {
            res.send(404, "404: Annonce non existante");
        } else {
            var annonceTitle = annonce.title;

            annonce.remove().then(annonce => {
                user.annonces.splice(user.annonces.indexOf(ad_id), 1);
                user.save().catch(err => {
                    console.log(err)
                });
            });

            var options = {
                from: "New Waste Life <newwastelife@gmail.com>",
                to: user.courriel,
                subject: 'Suppression de votre annonce : ' + annonceTitle,
                template: 'contact-delete-ad',
                context: {
                    user: {
                        lastname: user.lastname,
                        firstname: user.firstname
                    },
                    annonceTitle: annonceTitle
                }
            };

            var message = "";
            var messageType = "";

            transporter.sendMail(options, function (err, info) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(info);
                    res.redirect("/profile");
                }
            });
        }
    });
});

// Route qui permet de noter un utilisateur (acheteur)
router.get("/rating/:id", isAuthenticated, function (req, res) {
    const username = getUsernameFromCookie(req.cookies.jwt);
    var annonce_id = req.params.id;

    User.findOne({
            username: username
        })
        .populate({
            path: 'annonces',
            populate: {
                path: 'author'
            },
            match: {
                _id: annonce_id
            },
        })
        .exec((err, user) => {
            if (err) {
                console.log(err);
                return res.status(404).send("404: Annonce non existante");
            }
            res.render("rating", {
                user: user,
                annonce: user.annonces[0],
                err: ""
            });
        });
});

// Route qui valide et applique la note à l'utilisateur ciblé (l'acheteur)
router.post("/rating/:id/:rating", async function (req, res) {
    const pseudonyme = req.body.pseudonyme;
    const annonce_id = req.params.id;
    const user = await User.findOne({
        username: getUsernameFromCookie(req.cookies.jwt)
    });

    User.findOne({
        username: pseudonyme
    }, (err, buyer) => {
        if (err) {
            console.log(err);
            return res.status(500).send('Une erreur est survenue...');
        }
        if (buyer) {
            buyer.ratings.push(req.params.rating);
            buyer.save();
            Annonce.findById(annonce_id, (err, annonce) => {
                if (err) {
                    console.log(err);
                    return res.status(404).send("404: Annonce non existante");
                } else {
                    annonce.acheter = buyer.username;
                    annonce.save();
                }
            });
            return res.redirect('/profile')
        } else {
            Annonce.findById(annonce_id).populate('author').exec((err, annonce) => {
                if (err) {
                    console.log(err);
                    return res.status(404).send("404: Annonce non existante");
                }
                res.render("rating", {
                    user: user,
                    annonce: annonce,
                    err: "An error occured"
                });
            })
        }
    });
});

// Route qui permet de noter un utilisateur (vendeur)
router.get("/ratingAchat/:id", isAuthenticated, function (req, res) {
    Annonce.findById(req.params.id).populate('author').exec((err, annonce) => {
        if (err) {
            console.log(err);
            return res.status(404).send("404: Annonce non existante");
        }
        res.render("rating", {
            user: req.user,
            annonce: annonce,
            err: ""
        });
    });
});

// Route qui valide et applique la note à l'utilisateur ciblé (le vendeur)
router.post("/ratingAchat/:id/:rating", isAuthenticated, async function (req, res) {
    const annonce_id = req.params.id;

    Annonce.findById(annonce_id, (err, annonce) => {
        if (err) {
            console.log(err);
            return res.status(404).send("404: Annonce non existante");
        }
        User.findById(annonce.author, (err, seller) => {
            if (err) {
                console.log(err);
                return res.status(500).send('Une erreur est survenue...');
            }
            if (seller) {
                seller.ratings.push(req.params.rating);
                seller.save();
                res.redirect('/profile')
            } else {
                res.render("rating", {
                    user: req.user,
                    annonce: annonce,
                    err: "An error occured"
                });
            }
        })
    });
});

// Route qui ajoute en coup coeur une annonce
router.post("/favorite", isAuthenticated, function (req, res) {
    const user = req.user;
    if (!user.favorites.includes(req.body.annonce)) {
        user.favorites.push(req.body.annonce);
        user.save();
    }
    res.send();
});

// Route qui retire des coups coeurs une annonce
router.delete('/favorite', isAuthenticated, function (req, res) {
    const user = req.user;
    if (user.favorites.includes(req.body.annonce)) {
        user.favorites.pull(req.body.annonce);
        user.save();
    }
    res.send();
})

// Route qui permet d'afficher les infos sur l'annonce a modifier
router.get("/edit/ad/:id", isAuthenticated, function (req, res) {
    const id = req.params.id;
    Annonce.findById(id).populate('author').exec((err, annonce) => {
        if (err) {
            res.status(404).send("404: Annonce non existante");
        } else {
            res.render("edit", {
                annonce,
                user: req.user,
                action: `/edit/ad/${id}`
            });
        }
    });
});

// Route qui valide et modifie les informations d'une annonce
router.post('/edit/ad/:id', isAuthenticated, upload.array('adphoto', 3), function (req, res) {
    const title = req.body.title;
    const price = req.body.price;
    const description = req.body.description;
    const type = req.body.type;
    const photos = (req.files).map(file => file.filename);
    const changedPhotos = req.body.changedPhotos;
    const id = req.params.id

    Annonce.findById(id, function (err, annonce) {
        if (err) {
            console.log(err);
            return res.status(404).send("404: Annonce non existante");
        }
        if (title !== annonce.title && title) {
            annonce.title = title[0];
        }
        if (price !== annonce.price && price) {
            annonce.price = price[0];
        }
        if (description !== annonce.description && description) {
            annonce.description = description[0];
        }
        if (type !== annonce.type && type) {
            annonce.type = type[0];
        }
        if (photos) {
            if (annonce.photos.length > 0) {
                if (changedPhotos) {
                    if ((typeof changedPhotos) === 'string') {
                        let index = annonce.photos.indexOf(changedPhotos)
                        let tempPhotos = annonce.photos.map(photo => photo);
                        tempPhotos[index] = photos[0];
                        annonce.photos = tempPhotos;
                    } else {
                        let tempPhotos = annonce.photos.map(photo => photo);
                        for (let i = 0; i < photos.length; i++) {
                            let index = annonce.photos.indexOf(changedPhotos[i]);
                            tempPhotos[index] = photos[i];
                        }
                        annonce.photos = tempPhotos;
                    }
                }
                for (let i = 0; i < photos.length; i++) {
                    if (!annonce.photos.includes(photos[i])) {
                        annonce.photos.push(photos[i]);
                    }
                }
            } else {
                annonce.photos = photos;
            }
        }
        var verif = !descriptionIsValid(annonce.description);
        annonce.verif = verif;
        annonce.save();

        if (!verif) {
            /* Envoyer mail */
        }

        res.redirect('/confirmation/newAd/success')
    });
});

// Route qui affiche le résultat d'une recherche
router.post("/search", async function (req, res) {
    const query = req.body.query;
    const queries = query.split(" ");
    const username = getUsernameFromCookie(req.cookies.jwt);
    var results = [];

    const user = await User.findOne({
        username: username
    });

    for (let i = 0; i < queries.length; i++) {
        console.log(queries[i]);
        const adObject = await Annonce.find({
            "title": {
                "$regex": queries[i],
                "$options": "i"
            },
            "description": {
                "$regex": queries[i],
                "$options": "i"
            }
        }, (err, ads) => {
            if (err) {
                console.log(err);
            } else {
                results.push(ads);
            }
        });
    }

    res.render("searchResults", {
        results: results,
        user: user,
        query: query
    });
});

module.exports = router;