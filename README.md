﻿# NEW WASTE LIFE - PLATEFORME D'ÉCHANGE DE BIENS DE SERVICES #
  

### 1) CONFIGURATION ###

Installer Node.js : https://nodejs.org/en/download/

Installer express : ```$> npm install express```

Path fichier configuration : ```../config.py```

### 3) EXÉCUTION ###

Exécuter les lignes de commandes suivante afin de d'exécuter l'application  :
```
$> npm start
```
Allez à : http://127.0.0.1:5000/



