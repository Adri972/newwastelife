const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const path = require('path');
const cookieParser = require('cookie-parser');

// Connect to MongoDB
require('./config/dbconfig.js');

var indexRouter = require('./routes/index');

const app = express();

app.use('/uploads', express.static('uploads'));
app.use('/scripts', express.static(__dirname + '/node_modules/star-rating-svg/src/'));
app.use('/scripts', express.static(__dirname + '/node_modules/moment/'));
app.use('/scripts/css', express.static(__dirname + '/node_modules/pikaday/css'));
app.use('/scripts', express.static(__dirname + '/node_modules/jquery-input-mask-phone-number/dist/'));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.set("view engine", "ejs");

// Body parser middleware
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use('/', indexRouter);

// Passport middleware
app.use(passport.initialize());

// Passport Config
require('./config/passport')(passport);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));

module.exports = app;