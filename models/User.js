const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    courriel: {
        type: String,
        required: true
    },
    address: {
        type: String    
    },
    city: {
        type: String
    },
    province: {
        type: String
    },
    postalcode: {
        type: String
    },
    phone: {
        type: String
    },
    annonces: [
        {
            type: Schema.Types.ObjectId, 
            ref: 'Annonces'
        }
    ],
    favorites: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Annonces'
        }
    ],
    password: {
        type: String,
        required: true
    },
    ratings: [
        {
            type: Number
        }
    ]
});

mongoose.model('User', UserSchema, 'users');