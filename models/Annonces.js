const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const AnnoncesSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    photos: [{
        type: String
    }],
    date: {
        type: Date,
        default: Date.now
    },
    acheter:{
        type: String
    },
    verif:{
        type: Boolean,
        required: true
    }
});

mongoose.model('Annonces', AnnoncesSchema, 'annonces');