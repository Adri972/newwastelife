const mongoose = require('mongoose');
const db = require('./keys').mongoURI;

// Connect to MongoDB
mongoose
    .connect(db, { useNewUrlParser: true}) // mlab
    //.connect('mongodb://127.0.0.1:27017/newastelife', { useNewUrlParser: true}) // localhost
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log(err));

// All models goes here
require('../models/Annonces');
require('../models/User');
