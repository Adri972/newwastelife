$(document).ready(function (){
    $(".settings-ad").hover(function(){
        $(this).css("width", "140px");
        $(this).find('.btn-icon-wrapper .fa-cog').addClass("d-none");
        $(this).find('.btn-icon-wrapper button').removeClass("d-none");
    });

    $(".settings-ad").mouseleave(function(){
        $(this).css("width", "30px");
        $(this).find('.btn-icon-wrapper i').removeClass("d-none");
        $(this).find('.btn-settings-ad').addClass("d-none");
    });
    
    function favorite(instance) {
        $(instance).removeClass();
        $(instance).addClass('fas fa-heart');
    }

    function unfavorite(instance) {
        $(instance).removeClass();
        $(instance).addClass('pe-7s-like');
    }

    $(".card-body-ad .pe-7s-like").click(function() {
        var ad = $(this);
        let adId = ad.attr('id');

        setFavorite(ad, adId);
    });

    $(".card-body-ad .fa-heart").click(function() {
        var ad = $(this);
        let adId = ad.attr('id');

        setFavorite(ad, adId);
    });

    function setFavorite(ad, adId) {
        if(ad.hasClass('pe-7s-like')) {
            const data = {
                annonce: adId
            }
    
            $.ajax({
                method: 'post',
                url: '/favorite',
                data: data
            }).done(function (data) {
                if(data.error) {
                    location.href = data.error;
                }else {
                    favorite(ad);
                }
            }).fail(function () {
                alert("Oups! Un problème est survenu. Réessayez plus tard!");
            })
        } else {
            const data = {
                annonce: adId
            }

            $.ajax({
                method: 'delete',
                url: '/favorite',
                data: data
            }).done(function (data) {
                if(data.error) {
                    location.href = data.error;
                }else {
                    unfavorite(ad);
                }
            
            }).fail(function () {
                alert("Oups! Un problème est survenu. Réessayez plus tard!");
            });
        }
    }
});

