// Code goes here


    
    var map = null;
    var myMarker;
    var myLatlng;

    

    function initializeGMap(lat, lng) {
      myLatlng = new google.maps.LatLng(lat, lng);
        console.log
      var myOptions = {
        zoom: 13,
        zoomControl: true,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
  
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  
      myMarker = new google.maps.Marker({
        position: myLatlng
      });
      myMarker.setMap(map);
    }

    // Trigger map resize event after modal shown
    $('#myModal').on('shown.bs.modal', function() {
      google.maps.event.trigger(map, "resize");
      map.setCenter(myLatlng);
    });
 