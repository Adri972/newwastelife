$(document).ready(function () {
    $("#btn-prev-form-step2").click(function() {
        $("#formStep1").removeClass("d-none");
        $(".card-body").addClass("d-none");
        $("#formStep2").addClass("d-none");
    });

    $("#btn-next-form-step2").click(function() {
        $("#formStep2").addClass("d-none");
        $("#formStep3").removeClass("d-none");
    });

    $("#btn-prev-form-step3").click(function() {
        $("#formStep2").removeClass("d-none");
        $("#formStep3").addClass("d-none");
    });

    $("#btn-next-form-step3").click(function() {
        $("#formStep3").addClass("d-none");
        $("#formStep4").removeClass("d-none");
        initRecapInfos();
    });

    $("#btn-prev-form-step4").click(function() {
        $("#formStep3").removeClass("d-none");
        $("#formStep4").addClass("d-none");
    });

    $("#modifyInfos").click(function() {
        $("#formStep2").removeClass("d-none");
        $("#formStep3").addClass("d-none");
        $("#formStep4").addClass("d-none");
    });

    $('#radio-sale input').on('click', function(event) {
        var val = $(this).val();
        console.log(val);
        if(val == "Vendre"){
            $("#formRowPrice").removeClass("d-none");
        } else {
            $("#formRowPrice").addClass("d-none");
            $("#inp-price").val(0);
        }
        validation_form_step2();
    });
});

function ad_step2(type){
    $("#formStep1").addClass("d-none");
    $(".card-body").removeClass("d-none");
    $("#formStep2").removeClass("d-none");
    $("#selectType").val(type);
}

function validation_form_step2(){
    var type = document.getElementById("selectType").value;
    var title = document.getElementById("inp-title").value;
    var price = document.getElementById("inp-price").value;
    var description = document.getElementById("text-area-descritpion").value;

    if(type != "" && title != "" && price != "" && description != ""){
        document.getElementById("btn-next-form-step2").classList.remove("d-none");
    } else {
        document.getElementById("btn-next-form-step2").classList.add("d-none");
    }
}

function readURL(id) {
    var str_id = id.toString();
    var input = document.getElementById("customFile" + str_id);

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#photo" + id).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function initRecapInfos(){
    var type = $("#selectType").val();
    var title = $("#inp-title").val();
    var description = $("#text-area-descritpion").val();
    var price = $("#inp-price").val();
    var photo1 = $("#photo1").attr('src');
    var photo2 = $("#photo2").attr('src');
    var photo3 = $("#photo3").attr('src');

    console.log("Type: " + type );
    console.log("title: " + title );
    console.log("description: " + description );
    console.log("price: " + price );
    console.log("photo1: " + photo1 );
    console.log("photo2: " + photo2 );
    console.log("photo3: " + photo3 );

    $('#inp-type-check').val(type);
    $('#inp-title-check').val(title);
    $('#text-area-description-check').val(description);
    $('#inp-price-check').val(price);
    $('#photo1Check').attr('src', photo1);
    $('#photo2Check').attr('src', photo2);
    $('#photo3Check').attr('src', photo3);
}